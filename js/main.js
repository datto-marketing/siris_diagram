$(document).ready(function(){
	$('.icon').on('click', function(){
		//Close Previous Content
		$('.hardware .content.active').animate({
			'opacity': 0,
			'width':'0px'
		}, 0);
		$('.hardware .content.active').removeClass('active');

		//Remove Active Class From Line SVG's
		var lines = ["one", "two", "three", "four", "five", "six", "seven", "eight"];
		for (var i = 0; i<lines.length; i++){
			$('.line.'+lines[i]).attr('class', 'line '+lines[i]);
			$('.overlay.'+lines[i]).attr('class', 'overlay '+lines[i]);
		}

		var hardware = $(this).attr('id');
		var hardwareContent = $('.hardware .content.'+hardware);
		var line = $('.hardware .line.'+hardware);
		var overlay = $('.hardware .overlay.'+hardware);
		hardwareContent.addClass('active');
		line.attr('class','line '+hardware+' active');
		overlay.attr('class','overlay '+hardware+' active');
		if(hardwareContent.hasClass('active')){
			hardwareContent.animate({
				'opacity': 1,
				'width':'350px'
			}, 200);
		}
	});
});


	